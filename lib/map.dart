import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Map extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlutterMap(
        options: new MapOptions(
          center: new LatLng(35.757116, 51.414161),
          zoom: 13.0,
        ),
        layers: [
          new TileLayerOptions(
            urlTemplate: "https://api.tiles.mapbox.com/v4/"
                "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
            additionalOptions: {
              'accessToken':
              'pk.eyJ1IjoiaGFkaXNlaG1oZCIsImEiOiJjanpoZms4N3EwdTB2M29veTVidjdiYWNqIn0.RA6m-8lrOiq8yIDjFngyWQ',
              'id': 'mapbox.streets',
            },
          ),
          new MarkerLayerOptions(
            markers: [
              new Marker(
                width: 40.0,
                height: 40.0,
                point: new LatLng(35.757116, 51.414161),
                builder: (ctx) => new Container(
                  child: new  SvgPicture.asset(
                    "assets/icons/location.svg",
                    width: 30,
                    height: 30,

                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}