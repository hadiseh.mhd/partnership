import 'package:flutter/material.dart';

//import 'package:stopper/stopper.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:partnership/dropdown.dart';
import 'package:column_scroll_view/column_scroll_view.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

import './map.dart';
import 'timepicker.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  AnimatedContainer myChekedBox() {
    return AnimatedContainer(
      width: 20,
      height: 20,
      color: _selected ? Color(0xffffd198) : Color(0xffe0e0e0),
      child: Stack(
        children: <Widget>[
          Container(
            child: new SvgPicture.asset(
              "assets/icons/tick.svg",
              width: 30,
              height: 30,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  /////dropdownItems

  String dropdownValue = 'پارکور';

  final TextEditingController _passwordController = new TextEditingController();

  List _fields = ["والیبال", "گیتار", "تنیس", "پارکور", "نقاشی(آبرنگ)"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentField;

  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentField = _dropDownMenuItems[0].value;
    super.initState();
  }

  void changedDropDownItem(String selectedField) {
    print("Selected Field $selectedField");
    setState(() {
      _currentField = selectedField;
    });
  }

  void _erase() {
    setState(() {});
  }

  bool _selected = false;

  //////chekedboxItemes

  bool Val1 = false;
  bool Val2 = false;
  bool Val3 = false;
  bool Val4 = false;
  bool Val5 = false;
  bool Val6 = false;
  bool novitiate = false;
  bool professional = false;
  bool educator = false;

  Widget toolscheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
        ),
        Container(
          decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Checkbox(
            hoverColor: Color(0xffe0e0e0),
            activeColor: Color(0xffffd196),
            checkColor: Colors.white,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            value: boolValue,
            onChanged: (bool value) {
              setState(() {
                switch (title) {
                  case "دوچرخه":
                    Val1 = value;
                    break;
                  case "زانوبند":
                    Val2 = value;
                    break;
                  case "کلاه ایمنی":
                    Val3 = value;
                    break;
                  case "مچ بند":
                    Val4 = value;
                    break;
                  case "کف بند":
                    Val5 = value;
                    break;
                  case "عینک":
                    Val6 = value;
                    break;
                }
              });
            },
          ),
        )
      ],
    );
  }

  Widget rankingcheckbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
        ),
        Container(
          decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Checkbox(
            hoverColor: Color(0xffe0e0e0),
            activeColor: Color(0xffffd196),
            checkColor: Colors.white,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            value: boolValue,
            onChanged: (bool value) {
              setState(() {
                if (title == "مبتدی")
                  novitiate = value;
                else if (title == "حرفه ای")
                  professional = value;
                else if (title == "مربی") educator = value;
              });
            },
          ),
        )
      ],
    );
  }

  ///////datepicker
  DateTime _selectedDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        decoration: BoxDecoration(
            color: Color(0xffffd196),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50))),
        child: Container(
          color: Colors.white,
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(
                          Icons.arrow_forward,
                          color: Colors.black,
                        ),
                        onPressed: null),
                  ],
                ),
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                primary: false,
                floating: true,
                pinned: true,
                expandedHeight: 500,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image.asset(
                    'assets/images/partnership.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    index = 1;
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Column(children: <Widget>[
                        SizedBox(
                          height: 15,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'پاتنرشیپ',
                              style: TextStyle(
                                fontSize: 23,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'جنسیت',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 40,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            SizedBox(
                              width: 40,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selected = !_selected;
                                });
                              },
                              child: AnimatedContainer(
                                height: _selected ? 80 : 85,
                                width: _selected ? 80 : 85,
                                decoration: BoxDecoration(
                                  color: _selected
                                      ? Color(0xffe0e0e0)
                                      : Color(0xffffd196),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(28)),
                                ),
                                duration: Duration(milliseconds: 300),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    SvgPicture.asset(
                                      "assets/icons/icons-mars.svg",
                                      width: 30,
                                      height: 30,
                                      color: _selected
                                          ? Colors.black
                                          : Colors.white,
                                    ),
                                    Text(
                                      'مرد',
                                      style: TextStyle(
                                          color: _selected
                                              ? Colors.black
                                              : Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selected = !_selected;
                                });
                              },
                              child: AnimatedContainer(
                                height: _selected ? 85 : 80,
                                width: _selected ? 85 : 80,
                                decoration: BoxDecoration(
                                  color: _selected
                                      ? Color(0xffffd196)
                                      : Color(0xffe0e0e0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(28)),
                                ),
                                duration: Duration(milliseconds: 300),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    SvgPicture.asset(
                                      "assets/icons/icons-venus.svg",
                                      width: 30,
                                      height: 30,
                                      color: _selected
                                          ? Colors.white
                                          : Colors.black,
                                    ),
                                    Text(
                                      'زن',
                                      style: TextStyle(
                                          color: _selected
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 40,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'موقعیت مکانی',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 45,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 17,
                        ),
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              height: 150,
                              width: 300,
                              child: ClipRRect(
                                child: Map(),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'رشته',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                DropdownButtonHideUnderline(
                                  child: Row(children: <Widget>[
                                    IconButton(
                                        icon: Icon(
                                          Icons.expand_more,
                                          color: Colors.black,
                                        ),
                                        onPressed: null),
                                    new DropdownButton<String>(
                                      value: _currentField,
                                      items: _dropDownMenuItems,
                                      icon: Icon(Icons.expand_more,
                                          color: Color(0xffe0e0e0)),
                                      style: new TextStyle(color: Colors.black),
                                      onChanged: changedDropDownItem,
                                    ),
//                                    IconButton(icon: Icon(Icons.expand_more,color: Colors.black,), onPressed: null)
                                  ]),
                                ),
                                Text('انتخاب کنید'),
                              ],
                            ),
                          ),
                          height: 50,
                          width: 300,
                          decoration: BoxDecoration(
                              color: Color(0xffe0e0e0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              "فهرست تجهیزات",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 19,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 28,
                            )
                          ],
                        ),
                        SizedBox(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            SizedBox(
                              width: 25,
                            ),
                            Text(
                              "تجهیزاتی که پارتنر باید به همراه داشته باشد",
                              style: TextStyle(
                                color: Color(0xffb8b8b8),
                                fontSize: 13,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                CheckedBox(title: 'عینک'),
                                SizedBox(height: 5,),
                                CheckedBox(title: 'کلاه ایمنی'),
                                SizedBox(height: 5,),
                                CheckedBox(title: 'دوچرخه'),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                toolscheckbox("مچ بند", Val4),
                                toolscheckbox("کف بند", Val5),
                                toolscheckbox("زانوبند", Val2),
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'سطح',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            SizedBox(
                              width: 2,
                            ),
                            rankingcheckbox("مبتدی", novitiate),
                            rankingcheckbox("حرفه ای", professional),
                            rankingcheckbox("مربی", educator),
                            SizedBox(
                              width: 2,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'زمان',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 40,
                            ),
                          ],
                        ),

                        Container(
                          child: Stack(children: <Widget>[
                            Container(
                              child: MyHomePage(),
                              height: 290,
                              width: 150,
                            ),
                          ]),
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'تاریخ',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: 50,
                          width: 300,
                          decoration: BoxDecoration(
                              color: Color(0xffe0e0e0),
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              )),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                GestureDetector(
                                  child: SvgPicture.asset(
                                      "assets/icons/icons-calendar-alt.svg",
                                      width: 20,
                                      height: 20,
                                      color: Colors.black),
                                  onTap: _presentDatePicker,
                                ),
                                Text(
                                  'تاریخ موردنظر خورد را انتخاب کنید',
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'جستجو تا',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        DropDown(),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: 50,
                          width: 300,
                          decoration: BoxDecoration(
                              color: Colors.black87,
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              )),
                          child: Center(
                            child: Text(
                              'پیدا کن',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
//                      AnimatedContainer(duration:Duration(microseconds: 333),
//                      width: 20,
//                      height: 20,
//
//
//                      child:  SvgPicture.asset(
//                    "assets/icons/tick.svg",
//                    width: 5,
//                    height: 5,
//
//                    ),
//                        decoration: BoxDecoration(
//                            color: _selected ?Color(0xffffd198) : Color(0xffe0e0e0),
//                          borderRadius: BorderRadius.all(Radius.circular(5))
//                        ),
//                      ),

//                        AnimatedCrossFade(
//                            firstChild: GestureDetector(
//                                child: Container(
//                                  height: 20,
//                                  width: 20,
//                                  decoration: BoxDecoration(
//                                      color: Color(0xffe0e0e0),
//                                      borderRadius:
//                                          BorderRadius.all(Radius.circular(5))),
//                                ),
//                                onTap: () {
//                                  setState(() {
//                                    _selected = !_selected;
//                                  });
//                                }),
//                            secondChild: GestureDetector(
//                                child: Container(
//                                  height: 20,
//                                  width: 20,
//                                  decoration: BoxDecoration(
//                                      color: Color(0xffe0e0e0),
//                                      borderRadius:
//                                          BorderRadius.all(Radius.circular(5))),
//                                  child:
//                                      SvgPicture.asset('assets/icons/tick.svg'),
//                                ),
//                                onTap: () {
//                                  setState(() {
//                                    _selected = !_selected;
//                                  });
//                                }),
//                            crossFadeState: _selected
//                                ? CrossFadeState.showFirst
//                                : CrossFadeState.showSecond,
//                            duration: Duration(microseconds: 300)),
                        SizedBox(
                          height: 50,
                        ),

//                      Container(
//                        child: SvgPicture.asset(
//                          "assets/icons/tick.svg",
//                          width: 30,
//                          height: 30,
//                          color:
//                          _selected ? Colors.black : Colors.white,
//                        ),
//                        width: 25,
//                        height: 25,
//                      ),
//                      SizedBox(height: 30,)
                      ]),
                    );
                  },
                  childCount: 1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String field in _fields) {
      items.add(new DropdownMenuItem(
          value: field,
          child: new Text(
            field,
          )));
    }
    return items;
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
    print('...');
  }

  Row MyChekedBox(title, bool boolValue) {
    bool value = false;
    int isChanged = 0;

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(title),
        SizedBox(
          width: 5,
        ),
        GestureDetector(
          onTap: () {
            setState(() {
//              switch (title) {

//                case "دوچرخه":
//                  isChanged == 1 ? _selected = false : _selected = true;
//                  break;
//                case "زانوبند":
//                  Val2 = value;
//                  break;
//                case "کلاه ایمنی":
////                  _selected  ? _selected =false : _selected = true ;
//                  break;
//                case "مچ بند":
//                  Val4 = value;
//                  break;
//                case "کف بند":
//                  Val5 = value;
//                  break;
//                case "عینک":
////                  _selected = true;
////                  break;
//              }
            });
          },
          child: AnimatedContainer(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: _selected ? Color(0xffffd196) : Color(0xffe0e0e0),
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            duration: Duration(milliseconds: 100),
            child: _selected
                ? Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          child: SvgPicture.asset("assets/icons/tick.svg",
                              width: 12, height: 12, color: Colors.white),
                          width: 19,
                          height: 19,
                        ),
                      ),
                    ],
                  )
                : null,
          ),
        ),
      ],
    );
  }

//  Widget widget() {
//    return new TimePickerSpinner(
//      is24HourMode: false,
//      normalTextStyle: TextStyle(
//          fontSize: 24,
//          color: Colors.deepOrange
//      ),
//      highlightedTextStyle: TextStyle(
//          fontSize: 24,
//          color: Colors.yellow
//      ),
//      spacing: 50,
//      itemHeight: 80,
//      isForce2Digits: true,
//      onTimeChange: (time) {
//        setState(() {
//          _dateTime = time;
//        });
//      },
//    );
//  }

}

class CheckedBox extends StatefulWidget {
  final String title;

  CheckedBox({this.title});

  @override
  _CheckedBoxState createState() => _CheckedBoxState();
}

class _CheckedBoxState extends State<CheckedBox> {
  Color color = Color(0xFFE0E0E0);
  Widget child = null;
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
//      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(widget.title),
        SizedBox(
          width: 15,
        ),
        GestureDetector(
          onTap: () {
            counter++;
            setState(() {
              if (counter % 2 == 1) {
                color = Color(0xFFFFD198);
                child = Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        child: SvgPicture.asset("assets/icons/tick.svg",
                            width: 12, height: 12, color: Colors.white),
                        width: 19,
                        height: 19,
                      ),
                    ),
                  ],
                );
              } else {
                color = Color(0xFFE0E0E0);
                child = null;
              }
            });
          },
          child: AnimatedContainer(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            duration: Duration(milliseconds: 100),
            child: child,
          ),
        ),
      ],
    );
  }
}
