import 'package:flutter/material.dart';

class DropDown extends StatefulWidget {
  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  ////dropdownSearchingItems
  String dropdownMenuItem = 'فردا';
  List _fields = ["فردا", "یک هفته", "یک ماه", "دو ماه"];

//  List<DropdownMenuItem<String>> _dropDownSearchingItems;
//
//  String _currentField;

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentField;
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentField = _dropDownMenuItems[0].value;
    super.initState();
  }

  void changedDropDownItem(String selectedField) {
    print("Selected Field $selectedField");
    setState(() {
      _currentField = selectedField;
    });
  }

  void _erase() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 300,
      decoration: BoxDecoration(
//          color: Color(0xffe0e0e0),
          border: Border.all(
            color: Color(0xffe0e0e0),
          ),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: DropdownButtonHideUnderline(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
          IconButton(
              icon: Icon(
                Icons.expand_more,
                color: Colors.black,
              ),
              onPressed: ()=> DropDown()),
          SizedBox(width: 180,),
          new DropdownButton<String>(
            value: _currentField,
            items: _dropDownMenuItems,
            icon: Icon(Icons.expand_more, color: Colors.white),
            style: new TextStyle(color: Colors.black),
            onChanged: changedDropDownItem,
          ),
//                                    IconButton(icon: Icon(Icons.expand_more,color: Colors.black,), onPressed: null)
        ]),
      ),
    );
  }

//
//  List<DropdownMenuItem<String>> getDropDownSearchItems() {
//    List<DropdownMenuItem<String>> items = new List();
//    for (String period in _period) {
//      items.add(new DropdownMenuItem(
//          value: period,
//          child: new Text(
//            period,
//          )));
//    }
//    return items;
//  }
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String field in _fields) {
      items.add(new DropdownMenuItem(
          value: field,
          child: new Text(
            field,
          )));
    }
    return items;
  }
}
