import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class MyHomePage extends StatefulWidget {


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  DateTime _dateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return
      new Container(

        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            hourMinute15Interval(),
            new Container(
              margin: EdgeInsets.only(
                top: 20


              ),
              child: Center(
                child: new Text(
                  _dateTime.hour.toString().padLeft(2, '0') + ':' +
                      _dateTime.minute.toString().padLeft(2, '0') + ':' +
                      _dateTime.second.toString().padLeft(2, '0'),
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ),
          ],
        ),
      );
  }


  Widget hourMinute15Interval() {
    return new TimePickerSpinner(
      spacing: 20,
//      minutesInterval: 15,
      onTimeChange: (time) {
        setState(() {
          _dateTime = time;
        });
      },
    );
  }
}
