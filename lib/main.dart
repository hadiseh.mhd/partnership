import 'package:flutter/material.dart';
//import 'package:partnership/partnership.dart';
import 'package:partnership/home.dart';
import 'package:partnership/timepicker.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'partnership',
      theme: ThemeData(
//        buttonTheme: ButtonThemeData(
//          textTheme: ButtonTextTheme.primary
//
//        ),
//        primaryTextTheme:TextTheme(
//          title:  TextStyle(
//            color:  Color(0xffffd198),
//          )
//        ),
        primaryColor: Color(0xffffd198),
        accentColor: Color(0xffffd198),
        fontFamily: 'YekanBakhFaNum',
//        textTheme: TextTheme(
//         display:    TextStyle(
//           color: Color(0xffffd198),
//         ))
//
      ),
      home: Home(),
    );
  }
}
